// Copyright 2017 Janez Jež, Denis Lempl, Niko Pavlinek, Žiga Sever
//
// This file is part of Orp.
//
// Orp is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Orp is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
// License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Orp.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <string>

#include "Logger.hpp"

static const std::string RESET   = "\033[0m";
static const std::string GREEN   = "\033[32;1m";
static const std::string YELLOW  = "\033[33;1m";
static const std::string RED     = "\033[31;1m";
static const std::string BOLD    = "\033[1m";

// TODO (NP): I want to clean this up and make it a cleaner
// implementation.

//
// Izpiši vsa sporočila.
//
static void
print_all (const log_level level, const std::string msg)
{
    switch (level)
    {
    case INFO:
        std::cout << GREEN << "[ INFO ] " << RESET << msg << '\n';
        break;
    case WARNING:
        std::cout << YELLOW << "[ WARNING ] " << RESET << msg << '\n';
        break;
    case ERROR:
        std::cout << RED << "[ ERROR ] " << RESET << BOLD << msg << RESET << '\n';
        break;
    default:
        return;
    }
}

//
// Izpiši samo WARNING in ERROR sporočila.
//
static void
print_warning (const log_level level, const std::string msg)
{
    switch (level)
    {
    case WARNING:
        std::cout << YELLOW << "[ WARNING ] " << RESET << msg << '\n';
        break;
    case ERROR:
        std::cout << RED << "[ ERROR ] " << RESET << BOLD << msg << RESET << '\n';
        break;
    default:
        return;
    }
}

//
// Izpiši samo ERROR sporočila.
//
static void
print_error (const log_level level, const std::string msg)
{
    switch (level)
    {
    case ERROR:
        std::cout << RED << "[ ERROR ] " << RESET << BOLD << msg << RESET << '\n';
        break;
    default:
        return;
    }
}

Logger::Logger (void) :
    level (INFO)
{
}

Logger::Logger (const log_level l) :
    level (l)
{
}

//
// Nastavi nivo log sporočil.  Nivo določi ali se sporočilo prikazalo
// ali ne.
//
void
Logger::set_level (const log_level level)
{
    this->level = level;
}

//
// Vrne trenutno nastavljen log level.
//
const log_level
Logger::get_level (void) const
{
    return level;
}

//
// Izpiše podano sporočilo, če je istega nivoja kot trenutno
// nastavljen nivo.  Npr. če je nivo nastavljen na WARNING se
// prikažejo le WARNING in ERROR sporočila.
//
void
Logger::log (const log_level level, const std::string msg) const
{

    switch (this->level)
    {
    case NONE:
        return;
    case INFO:
        print_all (level, msg);
        break;
    case WARNING:
        print_warning (level, msg);
        break;
    case ERROR:
        print_error (level, msg);
        break;
    }
}
