// Copyright 2017 Janez Je�, Denis Lempl, Niko Pavlinek, �iga Sever
//
// This file is part of Orp.
//
// Orp is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Orp is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
// License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Orp.  If not, see <http://www.gnu.org/licenses/>.

#include "TextureManager.hpp"

//
// Nalozi datoteko na SDL_Texture
//
SDL_Texture * 
TextureManager::load_texture (const char *filename)
{
    //initialize SDL_Image
    int flags = IMG_INIT_JPG | IMG_INIT_PNG;
    int initted = IMG_Init (flags);

    
    if (!initted)
    {
        logger.log (ERROR, "SDL_Image faild to initalize");
        logger.log (ERROR, IMG_GetError());
        //exit (EXIT_FAILURE);                         //Problem ker program kr zapre brez kakega opozorila
    }

    SDL_Surface * temp_surface = nullptr;
    temp_surface = IMG_Load (filename);
    if (temp_surface == nullptr)
    {
        logger.log (ERROR, "Load sprite Error!");
        logger.log (ERROR, IMG_GetError());
        //exit (EXIT_FAILURE);                         //Problem ker program kr zapre brez kakega opozorila
    }
    SDL_Texture * tex = SDL_CreateTextureFromSurface (engine.get_renderer (), temp_surface);
    SDL_FreeSurface (temp_surface);

    IMG_Quit ();

    return tex;
}

//
// Narise teksturo na zaslon
//
void
TextureManager::draw (SDL_Texture * tex, SDL_Rect src, SDL_Rect dest)
{
    SDL_RenderCopy (engine.get_renderer (), tex, &src, &dest);
}
