// Copyright 2017 Janez Je�, Denis Lempl, Niko Pavlinek, �iga Sever
//
// This file is part of Orp.
//
// Orp is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Orp is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
// License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Orp.  If not, see <http://www.gnu.org/licenses/>.

#include "Map.hpp"





//
// Konstruktor rece koliksna je velikost kvadratkv
//
Map::Map ()
{
    dirt = TextureManager::load_texture ("../sprites/texture_dirt.bmp");            //value 0
    grass = TextureManager::load_texture ("../sprites/sky.png");                    //value 1
    water = TextureManager::load_texture ("../sprites/texture_dirt.png");           //value 2
    flowerRED = TextureManager::load_texture("../sprites/flowerRED.png");           //value 3
    flowerWHITE = TextureManager::load_texture("../sprites/flowerWHITE.png");       //value 4
    flowerWHITE2 = TextureManager::load_texture("../sprites/flowerWHITE2.png");     //value 5
    sun1 = TextureManager::load_texture("../sprites/sun1.png");                     //value 6
    sun2 = TextureManager::load_texture("../sprites/sun2.png");                     //value 7
    sun3 = TextureManager::load_texture("../sprites/sun3.png");                     //value 8
    sun4 = TextureManager::load_texture("../sprites/sun4.png");                     //value 9
    cigl = TextureManager::load_texture("../sprites/cigl.png");                     //value 10
    black = TextureManager::load_texture("../sprites/black.png");                   //value 11
    bakla1 = TextureManager::load_texture("../sprites/bakla1.png");                 //value 12
    bakla2 = TextureManager::load_texture("../sprites/bakla2.png");                 //value 13
    //load_map (lvl1);

    src.x = src.y = 0;
    src.w = dest.w = 32;
    src.h = dest.h = 32;
    dest.x = dest.y = 0;

}


//
// prekopira mapo v array
//
void 
Map::load_map (int arr[20][25])
{
    for (int row = 0; row < 20; row++)
    {
        for (int column = 0; column < 25; column++)
        {
            map[row][column] = arr[row][column];
        }
    }
}


//
//  Narise mapo na zaslon
//
void 
Map::draw_map ()
{
    int type = 0;

    for (int row = 0; row < 20; row++)
    {
        for (int column = 0; column < 25; column++)
        {
            type = map[row][column];
            
            dest.x = column * 32;
            dest.y = row * 32;

            switch (type)
            {
            case 0:
                TextureManager::draw (dirt, src, dest);
                break;
            case 1:
                TextureManager::draw (grass, src, dest);
                break;
            case 2:
                TextureManager::draw (water, src, dest);
                break;
            case 3:
                TextureManager::draw(flowerRED, src, dest);
                break;
            case 4:
                TextureManager::draw(flowerWHITE, src, dest);
                break;
            case 5:
                TextureManager::draw(flowerWHITE2, src, dest);
                break;
            case 6:
                TextureManager::draw(sun1, src, dest);
                break;
            case 7:
                TextureManager::draw(sun2, src, dest);
                break;
            case 8:
                TextureManager::draw(sun3, src, dest);
                break;
            case 9:
                TextureManager::draw(sun4, src, dest);
                break;
            case 10:
                TextureManager::draw(cigl, src, dest);
                break;
            case 11:
                TextureManager::draw(black, src, dest);
                break;
            case 12:
                TextureManager::draw(bakla1, src, dest);
                break;
            case 13:
                TextureManager::draw(bakla2, src, dest);
                break;
            default:
                break;
            }
        }
    }
}
