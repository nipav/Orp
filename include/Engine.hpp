// Copyright 2017 Janez Jež, Denis Lempl, Niko Pavlinek, Žiga Sever
//
// This file is part of Orp.
//
// Orp is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Orp is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
// License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Orp.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <iostream>

#include "SDL_Image/SDL_image.h"
#include "SDL2/SDL.h"

class Engine
{
private:
    bool is_running;
    SDL_Window *window;
    int width, height;
    SDL_Renderer *renderer;


public:
    Engine (void);
    ~Engine (void);

    void set_state (const bool);
    bool get_state (void) const;
    SDL_Renderer *get_renderer (void) const;
    int get_win_width (void) const;
    int get_win_height (void) const;

    void init (const char *, int, int, int, int, bool);
    void handle_events (void);
    void update (void);
    void render (void) const;
    void clean (void) const;


};
