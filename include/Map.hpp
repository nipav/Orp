// Copyright 2017 Janez Je�, Denis Lempl, Niko Pavlinek, �iga Sever
//
// This file is part of Orp.
//
// Orp is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Orp is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
// License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Orp.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "TextureManager.hpp"
#include "Logger.hpp"

class Map
{

private:
    
    SDL_Rect  src, dest;
    
    SDL_Texture *dirt;
    SDL_Texture *grass;
    SDL_Texture *water;
    SDL_Texture *flowerRED;
    SDL_Texture *flowerWHITE;
    SDL_Texture *flowerWHITE2;
    SDL_Texture *sun1;
    SDL_Texture *sun2;
    SDL_Texture *sun3;
    SDL_Texture *sun4;
    SDL_Texture *cigl;
    SDL_Texture *black;
    SDL_Texture *bakla1;
    SDL_Texture *bakla2;

public:
    Map (void);
    ~Map (void);

    void load_map (int arr[20][25]);
    void draw_map (void);

    int map[20][25];

};