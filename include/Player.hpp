// Copyright 2017 Janez Jež, Denis Lempl, Niko Pavlinek, Žiga Sever
//
// This file is part of Orp.
//
// Orp is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Orp is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
// License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Orp.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <iostream>

#include "SDL2/SDL.h"

#include "TextureManager.hpp"

class Player
{
private:
    SDL_Texture *texture;
    SDL_Rect *rect;
    int rectsize;
    float velx, vely;
    int frame;

public:
    Player (void);
    ~Player (void);

    void set_texture (const std::string);
    SDL_Texture *get_texture (void) const;
    SDL_Rect *get_rect (void) const;
    void set_rectsize (int);
    int get_rectsize (void) const;
    void set_velx (const float);
    float get_velx (void) const;
    void set_vely (const float);
    float get_vely (void) const;
    void update (void);

    void set_x (const int);
    int get_x (void) const;
    void set_y (const int);
    int get_y (void) const;
};
