// Copyright 2017 Janez Jež, Denis Lempl, Niko Pavlinek, Žiga Sever
//
// This file is part of Orp.
//
// Orp is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Orp is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
// License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Orp.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <iostream>

enum log_level { INFO = 0, WARNING, ERROR, NONE };

class Logger
{
private:
    log_level level;

public:
    Logger (void);
    Logger (const log_level);

    void set_level (const log_level);
    const log_level get_level (void) const;
    void log (const log_level, const std::string) const;
};
